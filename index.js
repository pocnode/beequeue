const Queue = require('bee-queue');
const queue  = new Queue('imports');

// creating job

const job = queue.createJob({x: 2, y: 3});
job.save();
// saved job

job.on('succeeded', (result) => {
  console.log(`Received result for job ${job.id}: ${result}`);
});

// processing jobs
queue.process(function (job, done) {
  console.log(`Processing job ${job.id}`);
  return done(null, job.data.x + job.data.y);
});